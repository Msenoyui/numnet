from sklearn import svm, preprocessing
import numpy as np
from sklearn.ensemble import BaggingClassifier
import time
import sys
sys.path.insert(0, '../')
#helper functions for plotting, data access, writing results
#needs to be after the sys.path.insrt to be read
import numnetHelper as helper

params = dict()
params['data'] = None
params['save_pred'] = True
params['check_eras'] = True
params['path'] = '../'

def run(params = params):
    if params['data'] is None:
        x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
    else:
        x_train, y_train, x_valid, y_valid, x_predict, ids = params['data']

    print(x_train.shape, y_train.shape)
    print(x_predict.shape)
    start = time.time()
    print("Start Scaling")
    x_train_scaled = preprocessing.scale(x_train)
    x_valid_scaled = preprocessing.scale(x_valid)
    x_predict_scaled = preprocessing.scale(x_predict)
    #clf = svm.SVC(probability=True, verbose=True)
    n_estimators= 160
    
    print("Init SVC")
    clf = BaggingClassifier(svm.SVC(verbose=False, probability=True), verbose=False, max_samples=1.0 / n_estimators, n_estimators=n_estimators, n_jobs=-1)
    print("Fitting")
    clf.fit(x_train_scaled, y_train)
    stop = time.time()
    seconds = (stop - start)
    print("Train Time: " + str(seconds) + ' seconds')
    
    print("Scoring")
    val = clf.predict(x_valid_scaled)
    print(np.mean(val==y_valid))
    start = time.time()
    print("Start Pred")
    pred = clf.predict_proba(x_predict_scaled)
    print(pred[:,1])
    stop = time.time()
    seconds = (stop - start)
    print("Pred Time: " + str(seconds) + ' seconds')
    helper.savePred(ids, pred[:,1], "SVM_SVC_BAG", params['path'])

    if params['check_eras']:    
        helper.checkEras(ids, pred[:,1], params['path'])

if __name__=="__main__":
    runSVM()
