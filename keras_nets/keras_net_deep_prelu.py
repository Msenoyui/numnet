#model functions
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from keras.layers.advanced_activations import PReLU
from keras.layers.normalization import BatchNormalization

#data imported as numpy arrays
import numpy as np
#used to allow saving/reading local files
import sys
sys.path.insert(0, '../')
#helper functions for plotting, data access, writing results
#needs to be after the sys.path.insrt to be read
import numnetHelper as helper

#setting params here to easily change and find
params = dict()
params['batch_size'] = 16384
params['epochs'] = 200
params['path'] = '../'
params['data'] = None
params['plot_loss_logloss'] = True
params['print_history_summary'] = True
params['save_pred'] = True
params['check_eras'] = True

def run(params = params):
    if params['data'] is None:
        x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
        params['data_in_dim'] = len(x_train[0])
    else:
        x_train, y_train, x_valid, y_valid, x_predict, ids = params['data']

    model = Sequential() 
    act = PReLU()

    model.add(Dense(units=2048, input_dim=params['data_in_dim']))
    #model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(.5))

    model.add(Dense(units=1024))
    #model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(.5))

    model.add(Dense(units=512))
    #model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(.5))

    model.add(Dense(units=256))
    #model.add(BatchNormalization())
    model.add(PReLU())
    model.add(Dropout(.5))

    model.add(Dense(units=128))
    #model.add(BatchNormalization())
    model.add(PReLU())


    model.add(Dense(units=64))
    #model.add(BatchNormalization())
    model.add(PReLU())


    model.add(Dense(units=32))
    #model.add(BatchNormalization())
    model.add(PReLU())


    model.add(Dense(units=16))
    #model.add(BatchNormalization())
    model.add(PReLU())


    model.add(Dense(units=1, activation='sigmoid'))


    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    earlyStop = EarlyStopping(monitor='loss', min_delta=0, patience=20, verbose=1)
    reduceLR = ReduceLROnPlateau(monitor='loss', patience=10, verbose=1, factor=.5)
    history = model.fit(x_train, y_train, verbose=1, epochs=params['epochs'], batch_size=params['batch_size'], validation_data=(x_valid, y_valid), callbacks=[earlyStop, reduceLR])

    prediction = model.predict(x_predict, verbose=1)

    if params['save_pred']:
        helper.savePred(ids, prediction, "keras_deep_prelu", params['path'])
    if params['print_history_summary']:
        helper.kerasHistorySummary(history)
    if params['plot_loss_logloss']:
        helper.kerasPlotHistory(history)
    if params['check_eras']:    
        helper.checkEras(ids, prediction, params['path'])

if __name__=="__main__":
    runNet()

