import pandas as pd
import numpy as np
import time
import glob, os
import matplotlib.pyplot as plt 
import warnings
import requests
from numerapi.numerapi import NumerAPI

'''
 copy past the follow at top of code. modify the '../' to fit your relative path
 sys.path.insert(0, '../')
 import numnetHelper as helper
'''

'''
    takes in any > 0 number of file names as input
    saves the mean of the probablilites to "mean_pred.csv"
'''
def combineCSV(files):
    probs = []
    # Load the data from the CSV files
    # Create an array of DataFrames
    for file in files:
        probs.append(pd.read_csv(file, usecols=['probability']))
    
    #Load Ids into DataFrame
    ids = pd.read_csv(file, usecols=['id'])
    
    #Concat to one DataFrame
    concat = pd.concat(probs, axis = 1)

    #Get Mean
    mean = pd.DataFrame(concat.mean(axis = 1))


    #Write Data to "mean_preds.csv"
    joined = pd.DataFrame(ids).join(mean)
    joined.columns = ['id', 'probability']
    print("Writing predictions to mean_preds.csv")
    joined.to_csv("mean_preds.csv", index=False)
    print('Done')

    
def combinePollingMeanCSV(files):
    probs = []
    # Load the data from the CSV files
    # Create an array of DataFrames
    for file in files:
        probs.append(pd.read_csv(file, usecols=['probability']).as_matrix())
    probNp = np.array(probs)
    probBin = probNp > .5
    #print(probBin[:,0])
    vote = (np.sum(probBin, axis=0)/probBin.shape[0] > .5)
    mask = (probBin == vote)
    PWMprobs = (np.average(probNp, axis = 0, weights=mask).flatten())

    #Load Ids into DataFrame
    ids = pd.read_csv(file, usecols=['id'])
    

    #Write Data to "mean_preds.csv"
    joined = pd.DataFrame(ids).join(pd.DataFrame(PWMprobs))
    joined.columns = ['id', 'probability']
    print("Writing predictions to mean_preds.csv")
    joined.to_csv("poll_weighted_mean_preds.csv", index=False)
    print('Done')


'''
    get data with training data split into eras
'''
def getDataSplitByEra(datapath):
    # Load the data from the CSV files
    training_data = pd.read_csv(datapath + 'dataset/numerai_training_data.csv', header=0)
    prediction_data = pd.read_csv(datapath + 'dataset/numerai_tournament_data.csv', header=0)

    # Transform the loaded CSV data into numpy arrays
    data = training_data.as_matrix()
    x_train = []
    y_train = []
    eras = []

    indices = [i for i, s in enumerate(list(training_data)) if 'feature' in s]
    for era in set(data[:,1]):
        eras.append(data[data[:,1] == era])
        x_train.append(eras[-1][:,indices])
        y_train.append(eras[-1][:,-1])
     
    features = [f for f in list(training_data) if "feature" in f]
    x_prediction = prediction_data[features]
    ids = np.array(prediction_data["id"])

    #get validation data
    label = prediction_data["data_type"]
    label_list = label.values.tolist()
    val_cout = 0
    for item in label_list:
      if item == "validation" :
        val_cout += 1

    #separate dataset
    x_valid = np.array(x_prediction[:val_cout].values.tolist())
    y_valid = np.array(prediction_data["target"][:val_cout])
    x_test = np.array(x_prediction.values.tolist())
    return x_train, y_train, x_valid, y_valid, x_test, ids
    
''' 
    return data as np arrays
    x_train, y_train, x_valid, y_valid, x_test, ids = helper.getData()
'''
def getData(datapath):

    # Load the data from the CSV files
    training_data = pd.read_csv(datapath + 'dataset/numerai_training_data.csv', header=0)
    prediction_data = pd.read_csv(datapath + 'dataset/numerai_tournament_data.csv', header=0)

    # Transform the loaded CSV data into numpy arrays
    features = [f for f in list(training_data) if "feature" in f]
    X = training_data[features]
    Y = training_data["target"]
    x_prediction = prediction_data[features]
    ids = np.array(prediction_data["id"])

    #get validation data
    label = prediction_data["data_type"]
    label_list = label.values.tolist()
    val_cout = 0
    for item in label_list:
      if item == "validation" :
        val_cout += 1

    #separate dataset
    x_train = np.array(X.values.tolist())
    y_train = np.array(Y)
    x_valid = np.array(x_prediction[:val_cout].values.tolist())
    y_valid = np.array(prediction_data["target"][:val_cout])
    x_test = np.array(x_prediction.values.tolist())
    return x_train, y_train, x_valid, y_valid, x_test, ids
  
#save predictions to predictions folder in proper format.
#can overwrite save path 
def savePred(ids, pred, name, path):
    #combine two arrays into one
    preds = np.stack((ids.flatten(), pred.flatten()), axis=1)
    
    #save arrays into csv with proper header
    np.savetxt(path + "predictions/"+name+"_predictions.csv", preds, delimiter=",",header="id,probability", comments="", fmt='%1s,%1f')
    print("saved to " + path + "predictions/"+name+"_predictions.csv")
    
    
def saveVotes(ids, pred, name, path):
    votes = np.stack((ids.flatten(),(pred > .5).flatten()), axis=1)
    
    np.savetxt(path +  "votes/"+name+"_votes.csv", votes, delimiter=",",header="id,votes", comments="", fmt='%1s,%1f')
    print("saved to " + path + "votes/"+name+"_votes.csv")

def votePredPoolingMeanCSV(votePath = "./votes", predPath = "./predictions", name="combinded_pred_with_voting", destination="./"):
    returnMessage = ""
    voteFiles = glob.glob(votePath+ "/*.csv")
    predFiles = glob.glob(predPath+ "/*.csv")
    print("pred files -", predFiles)
    print("vote files -", voteFiles)
    if(len(voteFiles) >= len(predFiles)):
        warnings.warn("You have too many vote files compared to prediction files.")
    
    probs = []
    votes = []
    for file in predFiles:
        probs.append(pd.read_csv(file, usecols=['probability']).as_matrix())
    for file in voteFiles:
        votes.append(pd.read_csv(file, usecols=['votes']).as_matrix())

    probNp = np.array(probs)
    voteNp = np.array(votes)
    
    probVotes = probNp > .5
    #voteNP = voteNP.reshape(voteNP.shape[0], voteNP.shape[1])
    #probVotes = probVotes.reshape(probVotes.shape[0], probVotes.shape[1])
    print(voteNp.shape)
    print(probVotes.shape)
    if voteNp.shape == (0,):
        allVotes = probVotes
    else:
        allVotes = np.concatenate((probVotes, voteNp), axis=0)
    tallyVotes = np.mean(allVotes, axis=0)
    agreementScore = np.mean(np.absolute((tallyVotes - .5)))
    print("Total Agreement Score = %.2f%%" % (100*agreementScore))
    returnMessage += ("\nTotal Agreement Score = %.2f%%" % (100*agreementScore))
    netAgreementScore = np.mean(np.absolute((np.mean(probNp,axis=0) - .5)))
    print("Net Agreement Score = %.2f%%" % (100*netAgreementScore))
    returnMessage += ("\nNet Agreement Score = %.2f%%" % (100*netAgreementScore))
    decidedVote = (tallyVotes > .5)
    
    mask = (probVotes == decidedVote)
    # average number of wins for a net
    # average net count for win, std
    # average number of wins for votes
    # number of time votes overruled nets
    colCounter = 0
    for file in predFiles:
        print("Analysis of", file)
        print("average number of wins %.2f%%" % (100*np.mean(mask[colCounter,:])))
        returnMessage += ('\n' + file + " average number of wins %.2f%%" % (100*np.mean(mask[colCounter,:])))
        colCounter += 1
    
    voteMask = (voteNp == decidedVote)
    colCounter = 0
    
    for file in voteFiles:
        print("Analysis of", file)
        print("average number of wins %.2f%%" % (100*np.mean(voteMask[colCounter,:])))
        returnMessage += ('\n' + file + " average number of wins %.2f%%" % (100*np.mean(voteMask[colCounter,:])))
        colCounter += 1
    
    print("Average of number of nets included %.2f%%" % (100*np.mean(mask)))
    returnMessage += ('\n ' + "Average of number of nets included %.2f%%" % (100*np.mean(mask)))
    print("STD of number of nets included %.2f%%" % (100*np.std(np.mean(mask, axis=0))))
    returnMessage += ('\n ' + "STD of number of nets included %.2f%%" % (100*np.std(np.mean(mask, axis=0))))
    overRuleCount = np.mean(mask, axis=0) < .5
    print("Number of times vote overruled nets %.2f%%" % (100*np.mean(overRuleCount)))
    returnMessage += ('\n ' + "Number of times vote overruled nets %.2f%%" % (100*np.mean(overRuleCount)))
    
    finalProbabilities = (np.average(probNp, axis = 0, weights=mask).flatten())
    
    #Load Ids into DataFrame
    ids = pd.read_csv(predFiles[0], usecols=['id'])
    

    #Write Data to "mean_preds.csv"
    joined = pd.DataFrame(ids).join(pd.DataFrame(finalProbabilities))
    joined.columns = ['id', 'probability']
    print("Writing predictions to", name, ".csv")
    joined.to_csv(destination + name + ".csv", index=False)
    returnMessage += "\nconsistancy - %.2f%%" % (100*checkEras(ids.as_matrix(), finalProbabilities, './'))
    print('Done')
    return returnMessage

# average number of wins for a net
# average net count for win, std
# average number of wins for votes
# number of time votes overruled nets

    
#
def kerasPlotHistory(history):
    plt.figure(1)  
   
 # summarize history for accuracy  
    
    plt.subplot(211)  
    plt.plot(history.history['acc'])
    if 'val_acc' in history.history.keys():
        plt.plot(history.history['val_acc'])  
    plt.title('model accuracy')  
    plt.ylabel('accuracy')  
    plt.xlabel('epoch')  
    plt.legend(['train', 'test'], loc='upper left')  
       
    # summarize history for loss  
       
    plt.subplot(212)  
    plt.plot(history.history['loss'])
    if 'val_acc' in history.history.keys():
        plt.plot(history.history['val_loss'])      
    plt.title('model loss')  
    plt.ylabel('loss')  
    plt.xlabel('epoch')  
    plt.legend(['train', 'test'], loc='upper left')
    plt.tight_layout()    
    #plt.show() 
    
def kerasHistorySummary(history):
    valData = 'val_acc' in history.history.keys()
    minLoss = np.min(history.history['loss']) 
    maxAcc = np.max(history.history['acc'])
    endLoss = history.history['loss'][-1]
    endAcc = history.history['acc'][-1]
    if valData:
        minValLoss = np.min(history.history['val_loss'])
        maxValAcc = np.max(history.history['val_acc'])
        endValLoss = history.history['val_loss'][-1]
        endValAcc = history.history['val_acc'][-1]
    print("min loss     - %5f | max acc      - %5f" % (minLoss, maxAcc))
    if valData:
        print("min val loss - %5f | max val acc  - %5f" % (minValLoss, maxValAcc))
    print("end loss     - %5f | end acc      - %5f" % (endLoss, endAcc))
    if valData:
        print("end val loss - %5f | end val acc  - %5f" % (endValLoss, endValAcc))
  
def checkErasOld(ids, pred, path):
    preds = np.stack((ids.flatten(), pred.flatten()), axis=1)
    tour_data = pd.read_csv(path+'dataset/numerai_tournament_data.csv', header=0)
    data = tour_data.as_matrix()
    val_data =  data[data[:,2] == "validation"]
    eras = []

    for era in set(val_data[:,1]):
        eras.append(val_data[val_data[:,1] == era])
    
    EraPercent = []
    print("testing", len(eras), "eras")
    for era in eras:
        print("testing ", era[0,1])
        correct = 0
        for prediction in era:
            #print(preds[preds[:,0] == prediction[0]][0][1] ,prediction[-1], ((preds[preds[:,0] == prediction[0]][0][1]) > .5) == prediction[-1])
            correct += int(((preds[preds[:,0] == prediction[0]][0][1]) > .5) == prediction[-1])     
        EraPercent.append(correct/len(era))
        print("acc - %.2f%%" % (100*EraPercent[-1]))
    consistancy = np.mean(np.array(EraPercent)>.5)
    print("Consistancy %.2f%%" % (100*consistancy))
    return consistancy
    
def checkEras(ids, pred, path):
    print("checking consistancy across eras")
    preds = np.stack((ids.flatten(), pred.flatten()), axis=1)
    tour_data = pd.read_csv(path+'dataset/numerai_tournament_data.csv', header=0)
    data = tour_data.as_matrix()
    val_data_all =  data[data[:,2] == "validation"]
    val_data = np.stack((val_data_all[:,0], val_data_all[:,1], val_data_all[:,-1]), axis = 1)
    if (np.mean(preds[:val_data.shape[0],0] == val_data[:,0])) != 1.0:
        print("ids shuffled, will use old method")
        checkErasOld(ids, pred, path)
        return
    predsNp = preds[:val_data.shape[0],1] > .5
    val_acc = predsNp == val_data[:,-1]
    unique_eras, counts = np.unique(val_data[:,1], return_counts=True)
    era_dict = dict(zip(unique_eras, counts))
    unique_eras_correct, counts_correct = np.unique(val_data[val_acc,1], return_counts=True)
    era_cor_dict = dict(zip(unique_eras_correct, counts_correct))
    all_acc = []
    for era in era_dict:
        acc = era_cor_dict[era]/era_dict[era]
        print(era, "- %.2f%%" % (100*acc), '-', acc>.5)
        all_acc.append(acc >.5)
    consistancy = np.average(all_acc)
    print("consistancy - %.2f%%" % (100*consistancy))
    return consistancy
    
def sendDiscordMessage(message = "Test Message"):
    discord_webhook = 'https://discordapp.com/api/webhooks/445684454959218698/5eMiWhq5imCY5GAVwg7_zZCddNQ0OVOlgY9ZYbs0G5zo9pN_ZjtifyDI8j96N3t8i4pq'

    data = {"content": message}

    requests.post(discord_webhook, data= data)

def downloadData():
    dataPath = "."
    napi = NumerAPI(verbosity="info")
    dl_succeeded = napi.download_current_dataset(dest_path=dataPath, dest_filename='dataset', unzip=True)
    print("download succeeded: " + str(dl_succeeded))
    print("cleaning up")
    os.remove("./dataset.zip")
    
if __name__=="__main__":
    sendDiscordMessage(votePredPoolingMeanCSV())