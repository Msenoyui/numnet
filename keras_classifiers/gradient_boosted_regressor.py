from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import log_loss
import numpy as np
import time
import sys

sys.path.insert(0, '../')

import numnetHelper as helper

#setting params here to easily change and find
params = dict()

params['data'] = None
params['path'] = '../'
params['save_pred'] = True
params['check_eras'] = True

#t=.4           n=1000          n_estimators= 100
#t=4.s         n=10000        n_estimators= 100
#t=70.s       n=100000      n_estimators= 100
#t=550s      n=all(~400k)  n_estimators= 100

#t=.5           n= 1000           n_estimators= 200
#t=8.4         n= 10000         n_estimators= 200
#t=146.93   n= 100000       n_estimators= 200


def run(params = params):
    if params['data'] is None:
        x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
    else:
        x_train, y_train, x_valid, y_valid, x_predict, ids = params['data']
    
    start = time.time()

    clf = GradientBoostingRegressor(verbose=1, n_estimators=200, learning_rate=.1, max_depth = 1)

    print("Fitting")
    clf.fit(x_train,y_train)

    stop = time.time()
    seconds = (stop - start)

    start = time.time()
    print("Train Time: " + str(seconds) + ' seconds')

    print("Scoring")
    val = clf.predict(x_valid)
    print("val logLoss-", log_loss(y_valid, val))
    print("val Acc-", np.mean((val>.5)==y_valid))

    print('Start Pred')
    pred = clf.predict(x_predict)
    print(pred)
    stop = time.time()
    seconds = (stop - start)
    print("Pred Time: " + str(seconds) + ' seconds')
    helper.savePred(ids, pred, "gradient_boosted_regressor", params['path'])

    if params['check_eras']:    
        helper.checkEras(ids, pred, params['path'])

if __name__=="__main__":
    runGBR()
