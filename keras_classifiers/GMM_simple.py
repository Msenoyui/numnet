from sklearn import mixture, preprocessing
import numpy as np
from sklearn.ensemble import BaggingClassifier
import time
import sys
sys.path.insert(0, '../')
#helper functions for plotting, data access, writing results
#needs to be after the sys.path.insrt to be read
import numnetHelper as helper

params = dict()
params['data'] = None
params['save_pred'] = True
params['check_eras'] = True
params['path'] = '../'

def run(params = params):
    if params['data'] is None:
        x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
    else:
        x_train, y_train, x_valid, y_valid, x_predict, ids = params['data']

    print(x_train.shape, y_train.shape)
    print(x_predict.shape)
    start = time.time()
    
    print("Init GMM")
    clf = mixture.GaussianMixture(2, verbose =1)
    print("Fitting")
    clf.fit(x_train, y_train)
    stop = time.time()
    seconds = (stop - start)
    
    print("Train Time: " + str(seconds) + ' seconds')
    print("Scoring")
    val = clf.predict(x_valid)
    print(np.mean(val==y_valid))
    print("Start Pred")
    start = time.time()
    prediction = np.array(clf.predict(x_predict))
    if  .5 > np.mean(val==y_valid):
        print("rescore", np.mean((1-val)==y_valid))
        prediction = 1-prediction

    
    stop = time.time()
    seconds = (stop - start)
    print("Pred Time: " + str(seconds) + ' seconds')
    
    if params['save_pred']:
        helper.saveVotes(ids, prediction, "GMM_simple", params['path'])
        
    if params['check_eras']:    
        helper.checkEras(ids, prediction, params['path'])

if __name__=="__main__":
    runGMM()

    
