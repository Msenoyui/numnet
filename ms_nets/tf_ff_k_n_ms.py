#!/usr/bin/env python

import pandas as pd
import numpy as np
import tensorflow as tf
import time
import math
import sys
import os.path
import csv
import random 
from z3c.batching.batch import Batch
random.seed(time.time())

#initialize variables
h1 = 3
h2 = 3
h3 = 3
j1 = 20
j2 = 10
j3 = 5

input_layer = 21
num_classes = 1
learning_rate = .001
batch_size = 1000
log_dir = '../log_ms'
prediction_path = "../ms_pred/predictions_ff_k_n_ms.csv"
max_steps = 50000
dropout = 1
decay_rate = .0001
is_train_global = True

def placeholder_inputs():
    features_placeholder = tf.placeholder(tf.float32, shape=[None, input_layer])
    target_placeholder = tf.placeholder(tf.int32, shape=[None])
    dropout = tf.placeholder(tf.float32)
    is_training = tf.placeholder(tf.bool)
    return features_placeholder, target_placeholder, dropout, is_training

def builder(features, input_size, layer_size, is_training, name_scope):
    with tf.name_scope(name_scope):
        weights = tf.Variable(tf.truncated_normal([input_size, layer_size], stddev=1.0 / math.sqrt(float(input_size))), name='weights')
        biases = tf.Variable(tf.zeros([layer_size]), name='biases')
        hidden = tf.nn.relu(tf.matmul(features, weights) + biases)
        return hidden

def feat_model(feature, is_training, name_scope):
    layer1 = builder(feature, 1, h1, is_training, name_scope + "_layer1")
    layer2 = builder(layer1, h1, h2, is_training, name_scope + "_layer2")
    layer3 = builder(layer2, h2, h3, is_training, name_scope + "_layer3")
    layer_out = builder(layer3, h3, 1, is_training, name_scope + "_layer_out")
    return layer_out

def model(features, dropout, is_training):
    f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17, f18, f19, f20, f21 = tf.split(features, 21, 1)
    out1 = feat_model(f1, is_training, "f1")
    out2 = feat_model(f2, is_training, "f2")
    out3 = feat_model(f3, is_training, "f3")
    out4 = feat_model(f4, is_training, "f4")
    out5 = feat_model(f5, is_training, "f5")
    out6 = feat_model(f6, is_training, "f6")
    out7 = feat_model(f7, is_training, "f7")
    out8 = feat_model(f8, is_training, "f8")
    out9 = feat_model(f9, is_training, "f9")
    out10 = feat_model(f10, is_training, "f10")
    out11 = feat_model(f11, is_training, "f11")
    out12 = feat_model(f12, is_training, "f12")
    out13 = feat_model(f13, is_training, "f13")
    out14 = feat_model(f14, is_training, "f14")
    out15 = feat_model(f15, is_training, "f15")
    out16 = feat_model(f16, is_training, "f16")
    out17 = feat_model(f17, is_training, "f17")
    out18 = feat_model(f18, is_training, "f18")
    out19 = feat_model(f19, is_training, "f19")
    out20 = feat_model(f20, is_training, "f20")
    out21 = feat_model(f21, is_training, "f21")
    out = tf.stack([out1, out2, out3, out4, out5, out6, out7, out8, out9, out10, out11, out12, out13, out14, out15, out16, out17, out18, out19, out20, out21], axis=1)
    
    with tf.name_scope('join_layer1'):
        weights = tf.Variable(tf.truncated_normal([input_layer, j1], stddev=1.0 / math.sqrt(float(input_layer))), name='weights')
        biases = tf.Variable(tf.zeros([j1]), name='biases')
        join_layer1 = tf.nn.relu(tf.matmul(out[:,:,0], weights) + biases)

    with tf.name_scope('join_layer2'):
        weights = tf.Variable(tf.truncated_normal([j1, j2], stddev=1.0 / math.sqrt(float(input_layer))), name='weights')
        biases = tf.Variable(tf.zeros([j2]), name='biases')
        join_layer2 = tf.nn.relu(tf.matmul(join_layer1, weights) + biases)

    with tf.name_scope('join_layer3'):
        weights = tf.Variable(tf.truncated_normal([j2, j3], stddev=1.0 / math.sqrt(float(input_layer))), name='weights')
        biases = tf.Variable(tf.zeros([j3]), name='biases')
        join_layer3 = tf.nn.relu(tf.matmul(join_layer2, weights) + biases)

    # Linear
    #
    with tf.name_scope('softmax_linear'):
        weights = tf.Variable(tf.truncated_normal([j3, num_classes], stddev=1.0 / math.sqrt(float(j3))), name='weights')
        biases = tf.Variable(tf.zeros([num_classes]), name='biases')
        logits = tf.nn.sigmoid(tf.matmul(join_layer3, weights) + biases)
    
    logits = tf.reshape(logits, [-1])
    mean, std = tf.nn.moments(logits,[0])
    tf.summary.scalar('std', std)
    tf.summary.scalar('mean', mean)
    return logits


def loss(logits, target):
    cross_entropy = tf.square(tf.cast(target, tf.float32) - tf.cast(logits, tf.float32))
    return tf.reduce_mean(cross_entropy, name='xentropy_mean')

def logloss(logits, target):
    loglost = tf.losses.log_loss(target, logits, reduction = "weighted_mean")
    return loglost# * (1 + tf.abs(tf.reduce_mean(logits)-.5))
#def loss(logits, target):
#    cross_entropy = tf.square(tf.square(tf.cast(target, tf.float32) - tf.cast(logits, tf.float32)))
#    return tf.reduce_mean(cross_entropy, name='xentropy_mean')

    #return tf.losses.log_loss(target, logits)

def evaluation(logits, targets):
    targets = tf.round(tf.cast(targets, tf.float32))
    logits = tf.round(tf.cast(logits, tf.float32))

    correct = tf.equal(logits, targets)

    return tf.reduce_sum(tf.cast(correct, tf.float32))


def fill_feed_dict(batch_features, batch_target, features_placeholder, target_placeholder, keep, dropout, is_training_placeholder, is_training=is_train_global):
    randnum = random.randint(0, batch_features.total-1)
    batch_features = batch_features.batches[randnum]
    batch_target = batch_target.batches[randnum]
    features_feed = list(batch_features)
    target_feed = list(batch_target)
    feed_dict = {
        features_placeholder: features_feed,
        target_placeholder: target_feed,
        keep: dropout,
        is_training_placeholder: is_training
    }

    return feed_dict

def train(loss):
    tf.summary.scalar('loss', loss)
    global_step = tf.Variable(0, name='global_step', trainable=False)
    decay_learn = tf.train.exponential_decay(learning_rate, global_step, max_steps, decay_rate)
    tf.summary.scalar('decay_learn', decay_learn)
    optimizer = tf.train.AdamOptimizer(decay_learn)

    
    train_op = optimizer.minimize(loss, global_step=global_step)

    return train_op


def do_eval(sess, eval_correct, loss, features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder, x, y):
   num_examples = len(x)
   feed_dict = {features_placeholder: x, target_placeholder: y, keep_placeholder: 1, is_training_placeholder:False}
   true_count = sess.run(eval_correct, feed_dict=feed_dict)
   precision = float(true_count) / num_examples
   logloss = sess.run([loss],feed_dict=feed_dict)
   print('examples: %d correct: %d  Precision: %0.05f Logloss: %0.06f' %
        (num_examples, true_count, precision, np.mean(logloss)))


def run_training(xbatch_train, ybatch_train, xbatch_valid, ybatch_valid, xtest, xtrain, ytrain, xvalid, yvalid):
    with tf.Graph().as_default():
        features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder = placeholder_inputs()
        logits = model(features_placeholder, keep_placeholder, is_training_placeholder)
        loss_val = loss(logits, target_placeholder)
        logloss_val = logloss(logits, target_placeholder)
        train_op = train(logloss_val)
        eval_correct = evaluation(logits, target_placeholder)
        summary = tf.summary.merge_all()
        init = tf.global_variables_initializer()
        saver = tf.train.Saver()
        sess = tf.Session()
        summary_writer = tf.summary.FileWriter(log_dir, sess.graph)
        sess.run(init)

        for step in xrange(max_steps):
            start_time = time.time()
            feed_dict = fill_feed_dict(xbatch_train, ybatch_train, features_placeholder, target_placeholder, keep_placeholder, dropout, is_training_placeholder)
            _, loss_value, logloss_value = sess.run([train_op, loss_val, logloss_val], feed_dict=feed_dict)

            duration = time.time() - start_time
            if step % 100 == 0:
                print('Step %d: loss = %.4f Logloss = %.4f (%.3f sec)' % (step, loss_value, logloss_value, duration))
                summary_str = sess.run(summary, feed_dict=feed_dict)
                summary_writer.add_summary(summary_str, step)
                summary_writer.flush()

            if (step + 1) % 1000 == 0 or (step + 1) == max_steps:
                checkpoint_file = os.path.join(log_dir, 'model.ckpt')
                saver.save(sess, checkpoint_file, global_step=step)
                print('Training Data Eval:')
                do_eval(sess, eval_correct, logloss_val, features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder, xtrain, ytrain)
                print('Validation Data Eval:')
                do_eval(sess, eval_correct, logloss_val, features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder, xvalid, yvalid)
                std_mean_print(sess.run([logits],  feed_dict = {features_placeholder: xtest, keep_placeholder: 1, is_training_placeholder:False}))                
        predictions = sess.run(logits, feed_dict = {features_placeholder: xtest, keep_placeholder: 1, is_training_placeholder:False})

        return predictions

def dataSq(data):
	return data

def std_mean_print(data):
    print('%0.05f mean, %0.05f std') % (np.mean(data), np.std(data))

def main():
    start = time.time()
    np.random.seed(0)   

    print 'Loading data...'
    # Load the data from the CSV files
    training_data = pd.read_csv('../dataset_8_9/numerai_training_data.csv', header=0)
    prediction_data = pd.read_csv('../dataset_8_9/numerai_tournament_data.csv', header=0)

    # Transform the loaded CSV data into numpy arrays
    features = [f for f in list(training_data) if "feature" in f]
    X = training_data[features]
    Y = training_data["target"]
    x_prediction = prediction_data[features]
    ids = prediction_data["id"]

    #get validation data
    label = prediction_data["data_type"]
    label_list = label.values.tolist()
    val_cout = 0
    for item in label_list:
      if item == "validation" :
        val_cout += 1

    #separate dataset
    x_train = dataSq(X[1:].values.tolist())
    y_train = Y[1:]
    x_valid = dataSq(x_prediction[1:val_cout].values.tolist())
    y_valid = prediction_data["target"][1:val_cout]
    x_test = dataSq(x_prediction.values.tolist()) 

    #create batches
    batch_features_train = Batch(x_train, size = batch_size)
    batch_target_train = Batch(y_train.values.tolist(), size = batch_size)
    batch_features_valid = Batch(x_valid, size = batch_size)
    batch_target_valid = Batch(y_valid.values.tolist(), size = batch_size)

    print 'Training...'
    predictions = run_training(batch_features_train, batch_target_train, batch_features_valid, batch_target_valid, x_test, x_train, y_train, x_valid, y_valid)

    print 'Training complete, predictions obtained'
    results_df = pd.DataFrame(data={'probability':predictions})
    joined = pd.DataFrame(ids).join(results_df)

    print "Writing predictions to " +  prediction_path
    joined.to_csv(prediction_path, index=False)
    print "Done"
    stop = time.time()
    min = (stop - start)//(60)
    sec = ((stop - start)%60)//1
    print('%d min %d sec') % (min, sec)
    std_mean_print(predictions)


if __name__ == '__main__':
    main()
