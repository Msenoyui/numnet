21/2/18
SVM_simple
	Currently trains in 5 min but takes 30 min to predict.
GMM_simple
    Added a gaussian mixture model. Only usefull for classification. It will poll 
    but not average in the final vote
Keras_simple_1
    Finding that a single relu layer of 2k is consisantly getting .9624-25
ELU activation function
    seems to need a huge dropout to prevent overfitting

22/2/18
    Experimented with different relu variations in the wide nets. Deesh did the same for the deep. 
    Found that ELU needs a huge drop out to prevent overfitting(.9 for wide)
    Found that PRelu seems to be a nice comprimize of relu and ELU(used .5 dropout)
    Tried decreasing the reduction for the reduceLRonPlateau to .5 or .2 and tried a patience of 
    5 to increase update rate to counter dicresed reduction.
    Need to write a function to compare predictons to see simularity.
    
