import csv
import tf_net_simple_ms
import random
import time
import numpy as np
import pandas as pd
random.seed(time.time())

start = time.time()
np.random.seed(0)

max_steps = 20000
batch_size = 400 
learning_rate = 0.1
decay_rate = 0.1


total_list = []

num_tests = 1

for i in range(7):
	for j in range(7):
		for k in range(3):
			print "*************TEST NUMBER " + str(num_tests) + " OUT OF " + str(7*7*3) + '*************'

			logloss, acc = tf_net_simple_ms.wrapper(learning_rate, batch_size, max_steps, decay_rate)
			total_list.append([logloss, acc, learning_rate, decay_rate, batch_size])
			batch_size = batch_size + 300
			num_tests += 1
			stop = time.time()
			minutes = (stop - start)//(60)
			sec = ((stop - start)%60)//1
			print '%d min %d sec' % (minutes, sec)

		decay_rate = decay_rate * 0.1
		batch_size = 100

	learning_rate = learning_rate * 0.1
	decay_rate = 1.0

total_list.sort()


print 'Writing values to csv file...'
results_df = pd.DataFrame(data=total_list)
results_df.to_csv("hyp_res.csv", index = False, header = ["logloss", "acc", "learning rate", "decay rate", "batch size"])


stop = time.time()
minutes = (stop - start)//(60)
sec = ((stop - start)%60)//1
print '%d min %d sec' % (minutes, sec)