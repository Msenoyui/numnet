#model functions
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
#data imported as numpy arrays
import numpy as np
#used to allow saving/reading local files
import sys
sys.path.insert(0, '../')
#helper functions for plotting, data access, writing results
#needs to be after the sys.path.insrt to be read
import numnetHelper as helper

raise Exception("Failed concept, Do not use this net")
x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getDataSplitByEra()


#setting params here to easily change and find
params = dict()
params['data_in_dim'] = len(x_train[0][0])
params['batch_size'] = 4096
params['epochs'] = 100

params['plot_loss_logloss'] = True
params['print_history_summary'] = True
params['save_pred'] = True

model = Sequential()
model.add(Dense(units=2048, activation='relu', input_dim=params['data_in_dim']))
model.add(Dropout(.5))
model.add(Dense(units=1024, activation='relu'))
model.add(Dense(units=1, activation='sigmoid'))


model.compile(loss='binary_crossentropy',
              optimizer='sgd',
              metrics=['accuracy'])


for e in range(5):
    print("epoch %i" % (e+1))
    batch_order = np.arange(len(x_train))
    np.random.shuffle(batch_order)
    for batch in batch_order:
        history = model.fit(x_train[batch], y_train[batch], verbose=0, batch_size=50000)
        
print("Validation")
correct = model.evaluate(x_valid, y_valid, verbose=0)
print(correct)

x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData()
print("Start Traditional")
earlyStop = EarlyStopping(monitor='val_loss', min_delta=0, patience=20, verbose=1)
reduceLR = ReduceLROnPlateau(verbose=1)
history = model.fit(x_train, y_train, verbose=2, epochs=params['epochs'], batch_size=params['batch_size'], validation_data=(x_valid, y_valid), callbacks=[earlyStop, reduceLR])


print("Validation final")   
correct = model.evaluate(x_valid, y_valid, verbose=1)
print(correct)
print("Predicting")
prediction = model.predict(x_predict, verbose=1)

if params['save_pred']:
    helper.savePred(ids, prediction, "keras_era_batched")

if params['print_history_summary']:
    helper.kerasHistorySummary(history)

if params['plot_loss_logloss']:
    helper.kerasPlotHistory(history)