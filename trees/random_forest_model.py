from sklearn.ensemble import RandomForestClassifier
import numpy as np
import time
import sys

sys.path.insert(0, '../')

import numnetHelper as helper

#setting params here to easily change and find
params = dict()

params['data'] = None
params['path'] = '../'
params['save_pred'] = True
params['check_eras'] = True

def runForest(params = params):
    if params['data'] is None:
        x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
    else:
        x_train, y_train, x_valid, y_valid, x_predict, ids = params['data']
    
    start = time.time()

    clf = RandomForestClassifier(n_jobs = -1, n_estimators = 100, max_features = 20)

    print("Fitting")
    clf.fit(x_train,y_train)

    stop = time.time()
    seconds = (stop - start)

    start = time.time()
    print("Train Time: " + str(seconds) + ' seconds')

    print("Scoring")
    val = clf.predict(x_valid)
    print(np.mean(val==y_valid))

    print('Start Pred')
    pred = clf.predict_proba(x_predict)
    print(pred[:,1])
    stop = time.time()
    seconds = (stop - start)
    print("Pred Time: " + str(seconds) + ' seconds')
    helper.savePred(ids, pred[:,1], "random_forest", params['path'])

    if params['check_eras']:    
        helper.checkEras(ids, pred[:,1], params['path'])

if __name__=="__main__":
    runForest()
