#!/usr/bin/env python

import pandas as pd
import numpy as np
import tensorflow as tf
import time
import math
import sys
import os.path
import csv
import random 
from z3c.batching.batch import Batch
random.seed(time.time())

#initialize variables
hl1 = 2000
hl2 = 1500
hl3 = 1000
hl4 = 1000
hl5 = 1000
hl6 = 750
hl7 = 500
hl8 = 250
hl9 = 125
hl10 = 50
input_layer = 441
num_classes = 1
learning_rate = .001
batch_size = 100
log_dir = '../log_ms'
prediction_path = "../ms_pred/predictions_sqIn_oneHot_ms.csv"
max_steps = 1000000
dropout = 1
decay_rate = .00001
is_train_global = True

def placeholder_inputs():
    features_placeholder = tf.placeholder(tf.float32, shape=[None, input_layer])
    target_placeholder = tf.placeholder(tf.int32, shape=[None])
    dropout = tf.placeholder(tf.float32)
    is_training = tf.placeholder(tf.bool)
    return features_placeholder, target_placeholder, dropout, is_training

def builder_BN_DO_TANH(input_size, layer_size, features, dropout, is_training, name_scope):
    with tf.name_scope(name_scope):
        weights = tf.Variable(tf.truncated_normal([input_size, layer_size], stddev=1.0 / math.sqrt(float(input_size))), name='weights')
        biases = tf.Variable(tf.zeros([layer_size]), name='biases')
        hidden = tf.contrib.layers.batch_norm(tf.matmul(features, weights) + biases, is_training = is_training)
        return tf.nn.dropout(tf.nn.tanh(hidden), dropout)


def model(features, dropout, is_training):
    #print features
    #feats = tf.contrib.layers.flatten(tf.matmul(tf.transpose(features), features))
    #print feats

    '''
    # Hidden 1
    with tf.name_scope('hidden1'):
        weights = tf.Variable(tf.truncated_normal([input_layer, hl1], stddev=1.0 / math.sqrt(float(input_layer))), name='weights')
        biases = tf.Variable(tf.zeros([hl1]), name='biases')
        hidden1 = tf.contrib.layers.batch_norm(tf.matmul(features, weights) + biases, is_training = is_training)
        hidden1 = tf.nn.dropout(tf.nn.tanh(hidden1), dropout)

    # Hidden 2
    with tf.name_scope('hidden2'):
        weights = tf.Variable(tf.truncated_normal([hl1, hl2], stddev=1.0 / math.sqrt(float(hl1))), name='weights')
        biases = tf.Variable(tf.zeros([hl2]), name='biases')
        hidden2 = tf.contrib.layers.batch_norm(tf.matmul(hidden1, weights) + biases, is_training = is_training)
        hidden2 = tf.nn.dropout(tf.nn.tanh(hidden2), dropout)
    # Hidden 3
    with tf.name_scope('hidden3'):
        weights = tf.Variable(tf.truncated_normal([hl2, hl3], stddev=1.0 / math.sqrt(float(hl2))), name='weights')
        biases = tf.Variable(tf.zeros([hl3]), name='biases')
        hidden3 = tf.contrib.layers.batch_norm(tf.matmul(hidden2, weights) + biases, is_training = is_training)
        hidden3 = tf.nn.dropout(tf.nn.tanh(hidden3), dropout)
    # Hidden 4
    with tf.name_scope('hidden4'):
        weights = tf.Variable(tf.truncated_normal([hl3, hl4], stddev=1.0 / math.sqrt(float(hl3))), name='weights')
        biases = tf.Variable(tf.zeros([hl4]), name='biases')
        hidden4 = tf.nn.tanh(tf.matmul(hidden3, weights) + biases)
    # Hidden 5
    with tf.name_scope('hidden5'):
        weights = tf.Variable(tf.truncated_normal([hl4, hl5], stddev=1.0 / math.sqrt(float(hl4))), name='weights')
        biases = tf.Variable(tf.zeros([hl5]), name='biases')
        hidden5 = tf.nn.tanh(tf.matmul(hidden4, weights) + biases)
    '''
    layer1 = builder_BN_DO_TANH(input_layer, hl1, features, dropout, is_training, 'hidden1')
    layer2 = builder_BN_DO_TANH(hl1, hl2, layer1, dropout, is_training, 'hidden2')
    layer3 = builder_BN_DO_TANH(hl2, hl3, layer2, dropout, is_training, 'hidden3')
    layer4 = builder_BN_DO_TANH(hl3, hl4, layer3, dropout, is_training, 'hidden4')
    layer5 = builder_BN_DO_TANH(hl4, hl5, layer4, dropout, is_training, 'hidden5')
    layer6 = builder_BN_DO_TANH(hl5, hl6, layer5, dropout, is_training, 'hidden6')
    layer7 = builder_BN_DO_TANH(hl6, hl7, layer6, dropout, is_training, 'hidden7')
    layer8 = builder_BN_DO_TANH(hl7, hl8, layer7, 1, is_training, 'hidden8')        
    layer9 = builder_BN_DO_TANH(hl8, hl9, layer8, 1, is_training, 'hidden9')
    layer10 = builder_BN_DO_TANH(hl9, hl10, layer9, 1, is_training,'hidden10')

    # Linear
    with tf.name_scope('softmax_linear'):
        weights = tf.Variable(tf.truncated_normal([hl10, num_classes], stddev=1.0 / math.sqrt(float(hl10))), name='weights')
        biases = tf.Variable(tf.zeros([num_classes]), name='biases')
        logits = tf.nn.sigmoid(tf.matmul(layer10, weights) + biases)
    
    logits = tf.reshape(logits, [-1])
    return logits

def snitch(features_placeholder):
    return features_placeholder

def loss(logits, target):
    cross_entropy = tf.square(tf.cast(target, tf.float32) - tf.cast(logits, tf.float32))
    return tf.reduce_mean(cross_entropy, name='xentropy_mean')

def logloss(logits, target):
    loglost = tf.losses.log_loss(target, logits, reduction = "weighted_mean")
    return loglost# * (1 + tf.abs(tf.reduce_mean(logits)-.5))
#def loss(logits, target):
#    cross_entropy = tf.square(tf.square(tf.cast(target, tf.float32) - tf.cast(logits, tf.float32)))
#    return tf.reduce_mean(cross_entropy, name='xentropy_mean')

    #return tf.losses.log_loss(target, logits)

def evaluation(logits, targets):
    targets = tf.round(tf.cast(targets, tf.float32))
    logits = tf.round(tf.cast(logits, tf.float32))

    correct = tf.equal(logits, targets)

    return tf.reduce_sum(tf.cast(correct, tf.float32))


def fill_feed_dict(batch_features, batch_target, features_placeholder, target_placeholder, keep, dropout, is_training_placeholder, is_training=is_train_global):
    randnum = random.randint(0, batch_features.total-1)
    batch_features = batch_features.batches[randnum]
    batch_target = batch_target.batches[randnum]
    features_feed = list(batch_features)
    target_feed = list(batch_target)
    feed_dict = {
        features_placeholder: features_feed,
        target_placeholder: target_feed,
        keep: dropout,
        is_training_placeholder: is_training
    }

    return feed_dict

def train(loss):
    tf.summary.scalar('loss', loss)
    global_step = tf.Variable(0, name='global_step', trainable=False)
    decay_learn = tf.train.exponential_decay(learning_rate, global_step, max_steps, decay_rate)

    optimizer = tf.train.AdamOptimizer(decay_learn)

    
    train_op = optimizer.minimize(loss, global_step=global_step)

    return train_op


def do_eval(sess, eval_correct, loss, features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder, x, y):
   num_examples = len(x)
   feed_dict = {features_placeholder: x, target_placeholder: y, keep_placeholder: 1, is_training_placeholder:False}
   true_count = sess.run(eval_correct, feed_dict=feed_dict)
   precision = float(true_count) / num_examples
   logloss = sess.run([loss],feed_dict=feed_dict)
   print('examples: %d correct: %d  Precision: %0.05f Logloss: %0.06f' %
        (num_examples, true_count, precision, np.mean(logloss)))


def run_training(xbatch_train, ybatch_train, xbatch_valid, ybatch_valid, xtest, xtrain, ytrain, xvalid, yvalid):
    with tf.Graph().as_default():
        features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder = placeholder_inputs()
        stiches = snitch(features_placeholder)
        logits = model(features_placeholder, keep_placeholder, is_training_placeholder)
        loss_val = loss(logits, target_placeholder)
        logloss_val = logloss(logits, target_placeholder)
        train_op = train(logloss_val)
        eval_correct = evaluation(logits, target_placeholder)
        summary = tf.summary.merge_all()
        init = tf.global_variables_initializer()
        saver = tf.train.Saver()
        sess = tf.Session()
        summary_writer = tf.summary.FileWriter(log_dir, sess.graph)
        sess.run(init)

        for step in xrange(max_steps):
            start_time = time.time()
            feed_dict = fill_feed_dict(xbatch_train, ybatch_train, features_placeholder, target_placeholder, keep_placeholder, dropout, is_training_placeholder)
            _, loss_value, logloss_value = sess.run([train_op, loss_val, logloss_val], feed_dict=feed_dict)

            duration = time.time() - start_time
            if step % 100 == 0:
                print('Step %d: loss = %.4f Logloss = %.4f (%.3f sec)' % (step, loss_value, logloss_value, duration))
                #print sess.run([stiches], feed_dict=feed_dict)
                summary_str = sess.run(summary, feed_dict=feed_dict)
                summary_writer.add_summary(summary_str, step)
                summary_writer.flush()

            if (step + 1) % 1000 == 0 or (step + 1) == max_steps:
                checkpoint_file = os.path.join(log_dir, 'model.ckpt')
                saver.save(sess, checkpoint_file, global_step=step)
                print('Training Data Eval:')
                do_eval(sess, eval_correct, logloss_val, features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder, xtrain, ytrain)
                print('Validation Data Eval:')
                do_eval(sess, eval_correct, logloss_val, features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder, xvalid, yvalid)
                print sess.run([logits],  feed_dict=feed_dict)[0]                
        predictions = sess.run(logits, feed_dict = {features_placeholder: xtest, keep_placeholder: 1, is_training_placeholder:False})

        return predictions

def dataSq(data):
    out = []
    for line in data:
        lineTall = np.array(line)
        lineTall.shape = (1,21)
        lineFlat = np.array(line)
        lineFlat.shape = (21,1)
        dot = np.dot(lineFlat, lineTall)
        dot.shape = (441)
        out.append(dot)
    return out

def main():
    start = time.time()
    np.random.seed(0)   

    print 'Loading data...'
    # Load the data from the CSV files
    training_data = pd.read_csv('../dataset_7_20/numerai_training_data_7_20.csv', header=0)
    prediction_data = pd.read_csv('../dataset_7_20/numerai_tournament_data_7_20.csv', header=0)

    # Transform the loaded CSV data into numpy arrays
    features = [f for f in list(training_data) if "feature" in f]
    X = training_data[features]
    Y = training_data["target"]
    x_prediction = prediction_data[features]
    ids = prediction_data["id"]

    #get validation data
    label = prediction_data["data_type"]
    label_list = label.values.tolist()
    val_cout = 0
    for item in label_list:
      if item == "validation" :
        val_cout += 1

    #separate dataset
    x_train = dataSq(X[1:].values.tolist())
    y_train = Y[1:]
    x_valid = dataSq(x_prediction[1:val_cout].values.tolist())
    y_valid = prediction_data["target"][1:val_cout]
    x_test = dataSq(x_prediction.values.tolist()) 

    #create batches
    batch_features_train = Batch(x_train, size = batch_size)
    batch_target_train = Batch(y_train.values.tolist(), size = batch_size)
    batch_features_valid = Batch(x_valid, size = batch_size)
    batch_target_valid = Batch(y_valid.values.tolist(), size = batch_size)

    print 'Training...'
    predictions = run_training(batch_features_train, batch_target_train, batch_features_valid, batch_target_valid, x_test, x_train, y_train, x_valid, y_valid)

    print 'Training complete, predictions obtained'
    results_df = pd.DataFrame(data={'probability':predictions})
    joined = pd.DataFrame(ids).join(results_df)

    print "Writing predictions to " +  prediction_path
    joined.to_csv(prediction_path, index=False)
    print "Done"
    stop = time.time()
    min = (stop - start)//(60)
    sec = ((stop - start)%60)//1
    print('%d min %d sec') % (min, sec)
    print('%0.05f mean, %0.05f std') % (np.mean(predictions), np.std(predictions))


if __name__ == '__main__':
    main()
