import sys
import os
import glob
import numnetHelper as helper
from keras import backend as K

sys.path.insert(0, './')
import numnetHelper as helper
sys.path.insert(0, './keras_nets')
import keras_net_simple as ns
import keras_net_deep_elu as  de
import keras_net_deep_leaky as dl
import keras_net_deep_prelu as dp
import keras_net_wide_ELU as we
import keras_net_wide_PRelu as wp
import keras_net_wide_leaky as wl
sys.path.insert(0, './keras_classifiers')
import GMM_simple as gmm
import gradient_boosted_regressor as gboost
import kmeans_simple as kmeans
import SVM_SCV as svm
import random_forest_model as rforest


helper.downloadData()
params = dict()
params['batch_size'] = 16384
params['epochs'] = 200
params['path'] = './'
params['data'] = helper.getData(params['path'])
params['plot_loss_logloss'] = False
params['print_history_summary'] = True
params['save_pred'] = True
params['check_eras'] = True
x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
params['data_in_dim'] = len(x_train[0])
nets = [ns, de, dl, dp, we, wp, wl, gmm, gboost, kmeans, svm, rforest]
for net in nets:
    net.run(params)
    K.clear_session()
    
analysis = helper.votePredPoolingMeanCSV()
helper.sendDiscordMessage(analysis)