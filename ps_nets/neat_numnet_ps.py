
from __future__ import print_function
import os
import neat
import visualize
import pandas as pd
import numpy as np
import csv
from z3c.batching.batch import Batch

# 2-input XOR inputs and expected outputs.
xor_inputs = [(0.0, 0.0), (0.0, 1.0), (1.0, 0.0), (1.0, 1.0)]
xor_outputs = [   (0.0,),     (1.0,),     (1.0,),     (0.0,)]

#*****READ DATA*****
start = time.time()
print 'Loading data...'
# Load the data from the CSV files
training_data = pd.read_csv('../dataset_7_27/numerai_training_data.csv', header=0)
prediction_data = pd.read_csv('../dataset_7_27/numerai_tournament_data.csv', header=0)

# Transform the loaded CSV data into numpy arrays
features = [f for f in list(training_data) if "feature" in f]
X = training_data[features]
Y = training_data["target"]
x_prediction = prediction_data[features]
ids = prediction_data["id"]

#get validation data
label = prediction_data["data_type"]
label_list = label.values.tolist()
val_cout = 0
for item in label_list:
  if item == "validation" :
    val_cout += 1

#separate dataset
x_train = X[1:]
y_train = Y[1:]
x_train = np.array(x_train)
y_train = np.array(y_train)
x_valid = x_prediction[1:val_cout]
y_valid = prediction_data["target"][1:val_cout]
x_test = x_prediction 

#create batches
batch_features_train = Batch(x_train.values.tolist(), size = batch_size)
batch_target_train = Batch(y_train.values.tolist(), size = batch_size)
batch_features_valid = Batch(x_valid.values.tolist(), size = batch_size)
batch_target_valid = Batch(y_valid.values.tolist(), size = batch_size)


def eval_genomes(genomes, config):
    for genome_id, genome in genomes:
        genome.fitness = 4.0
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        for x, y in zip(xtrain, ytrain):
            output = net.activate(x)
            genome.fitness -= (output[0] - y[0]) ** 2


def run(config_file):
    # Load configuration.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)

    # Create the population, which is the top-level object for a NEAT run.
    p = neat.Population(config)

    # Add a stdout reporter to show progress in the terminal.
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(5))

    # Run for up to 300 generations.
    winner = p.run(eval_genomes, 300)

    # Display the winning genome.
    print('\nBest genome:\n{!s}'.format(winner))

    # Show output of the most fit genome against training data.
    print('\nOutput:')
    winner_net = neat.nn.FeedForwardNetwork.create(winner, config)
    for x, y in zip(xtrain, ytrain):
        output = winner_net.activate(x)
        print("input {!r}, expected output {!r}, got {!r}".format(x, y, output))

    node_names = {-1:'A', -2: 'B', 0:'A XOR B'}
    visualize.draw_net(config, winner, True, node_names=node_names)
    visualize.plot_stats(stats, ylog=False, view=True)
    visualize.plot_species(stats, view=True)

    p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-4')
    p.run(eval_genomes, 10)


if __name__ == '__main__':
    # Determine path to configuration file. This path manipulation is
    # here so that the script will run successfully regardless of the
    # current working directory.
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config-feedforward')
    run(config_path)