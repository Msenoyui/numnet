import numpy as np
from sklearn.cluster import KMeans
import time
import sys
sys.path.insert(0, '../')
#helper functions for plotting, data access, writing results
#needs to be after the sys.path.insrt to be read
import numnetHelper as helper

params = dict()
params['data'] = None
params['save_pred'] = True
params['check_eras'] = True
params['path'] = '../'

def run(params = params):
    if params['data'] is None:
        x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
    else:
        x_train, y_train, x_valid, y_valid, x_predict, ids = params['data']

    start = time.time()
    
    print("Init classifier")
    clf = KMeans(n_clusters = 2, n_jobs = -1, n_init = 50, max_iter = 3000)
    print("Fitting")
    clf.fit(x_train, y_train)
    stop = time.time()
    seconds = (stop - start)
    
    print("Train Time: " + str(seconds) + ' seconds')
    print("Scoring")
    val = clf.predict(x_valid)
    print(np.mean(val==y_valid))
    print("Start Pred")
    start = time.time()
    prediction = np.array(clf.predict(x_predict))
    
    stop = time.time()
    seconds = (stop - start)
    print("Pred Time: " + str(seconds) + ' seconds')
    
    if params['save_pred']:
        helper.saveVotes(ids, prediction, "KMeans_simple", params['path'])
        
    if params['check_eras']:    
        helper.checkEras(ids, prediction, params['path'])

if __name__=="__main__":
    runKmeans()

    
