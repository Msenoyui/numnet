from sklearn.neighbors import KNeighborsClassifier
import numpy as np
import time
import sys

sys.path.insert(0, '../')

import numnetHelper as helper

#setting params here to easily change and find
params = dict()

params['data'] = None
params['path'] = '../'
params['save_pred'] = True
params['check_eras'] = True

def runKNN(params = params):
    if params['data'] is None:
        x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
    else:
        x_train, y_train, x_valid, y_valid, x_predict, ids = params['data']

    start = time.time()

    knn = KNeighborsClassifier(n_neighbors = 5, n_jobs = -1)

    print("Fitting")
    knn.fit(x_train, y_train)

    stop = time.time()
    seconds = (stop - start)

    start = time.time()
    print("Train Time: " + str(seconds) + ' seconds')

    print("Scoring")
    val = knn.predict(x_valid)
    print(np.mean(val==y_valid))
    print("Start Pred")
    pred = knn.predict(x_predict)
    print(pred)
    stop = time.time()
    seconds = (stop - start)
    print("Pred Time: " + str(seconds) + ' seconds')
    helper.saveVotes(ids, pred, "knn_simple", params['path'])

    if params['check_eras']:    
        helper.checkEras(ids, pred, params['path'])

if __name__=="__main__":
    runKNN()