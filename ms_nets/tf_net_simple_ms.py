#!/usr/bin/env python

import pandas as pd
import numpy as np
import tensorflow as tf
import time
import math
import sys
import os
import errno
import csv
import random 
from z3c.batching.batch import Batch
random.seed(time.time())

#initialize variables
hl1 = 1000
hl2 = 500
hl3 = 100
hl4 = 20
hl5 = 4
hl6 = 500
hl7 = 400
hl8 = 300
hl9 = 200
hl10 = 100
input_layer = 50
num_classes = 1
layer_sizes =  [hl10] * 10#[hl1, hl2, hl3, hl4, hl5]
log_dir = '../log_ms'
prediction_path = '../ms_pred/predictions_simple_tf.csv'
datapath = '../dataset_2_18_18/'

learning_rate = .1
decay_rate = .00001
batch_size = 500
max_steps = 2000
g_step = 0
g_logloss = 0.0
g_acc = 0.0
best_step = 0
best_logloss = 100
dropoutLayerCount = 1
dropoutKeep = .75


def placeholder_inputs():
    features_placeholder = tf.placeholder(tf.float32, shape=[None, input_layer])
    target_placeholder = tf.placeholder(tf.int32, shape=[None])
    return features_placeholder, target_placeholder

def layer_stacker(input_size, features, layers = layer_sizes):
    name_scope = 'layer_' + str(len(layer_sizes) - len(layers))
    with tf.name_scope(name_scope):
        weights = tf.Variable(tf.truncated_normal([input_size, layers[0]], stddev=1.0 / math.sqrt(float(input_size))), name='weights')
        biases = tf.Variable(tf.zeros([layers[0]]), name='biases')
        hidden = tf.nn.relu6(tf.matmul(features, weights) + biases)
    if(len(layer_sizes) - len(layers) < dropoutLayerCount):
    	hidden = tf.nn.dropout(hidden, dropoutKeep)
    if(len(layers) <= 1):
        return hidden
    else:
        return layer_stacker(layers[0], hidden, layers[1:])

def model(features):
    layer_out = layer_stacker(input_layer, features, layer_sizes)

    # Linear
    with tf.name_scope('softmax_linear'):
        weights = tf.Variable(tf.truncated_normal([layer_sizes[-1], num_classes], stddev=1.0 / math.sqrt(float(layer_sizes[-1]))), name='weights')
        biases = tf.Variable(tf.zeros([num_classes]), name='biases')
        logits = tf.nn.sigmoid(tf.matmul(layer_out, weights) + biases)
    
    logits = tf.reshape(logits, [-1])
    mean, std = tf.nn.moments(logits,[0])
    tf.summary.scalar('std', std)
    tf.summary.scalar('mean', mean)
    return logits

def loss(logits, target):
    cross_entropy = tf.square(tf.square(tf.cast(target, tf.float32) - tf.cast(logits, tf.float32)))
    return tf.reduce_mean(cross_entropy, name='xentropy_mean')

def logloss(logits, target):
    loglost = tf.losses.log_loss(target, logits)
    return loglost

def evaluation(logits, targets):
    targets = tf.round(tf.cast(targets, tf.float32))
    logits = tf.round(tf.cast(logits, tf.float32))
    correct = tf.equal(logits, targets)
    cor_avg = tf.reduce_sum(tf.cast(correct, tf.float32))
    tf.summary.scalar('cor_avg', cor_avg)
    return cor_avg

def fill_feed_dict(x_in, y_in, features_placeholder, target_placeholder):
    length = len(x_in) - 1
    x_out = []
    y_out = []
    if len(x_in) != len(y_in):
        raise ValueError("invalid dataset: data/pred len don't match", len(x_in), len(y_in))
    for x in range(0, batch_size):
        randnum = random.randint(0, length)
        x_out.append(x_in[randnum])
        y_out.append(y_in[randnum])
    feed_dict = {
        features_placeholder: x_out,
        target_placeholder: y_out
    }

    return feed_dict

def train(loss):
    tf.summary.scalar('loss', loss)
    global_step = tf.Variable(0, name='global_step', trainable=False)
    decay_learn = tf.train.exponential_decay(learning_rate, global_step, max_steps, decay_rate)
    decay_learn = learning_rate * float(max_steps-g_step)/float(max_steps)
    tf.summary.scalar('learn_rate', decay_learn)
    optimizer = tf.train.GradientDescentOptimizer(decay_learn)
    train_op = optimizer.minimize(loss, global_step=global_step)
    return train_op


def do_eval(typeIn, sess, eval_correct, loss, features_placeholder, target_placeholder, x, y):
   global g_acc, g_logloss
   num_examples = len(x)
   feed_dict = {features_placeholder: x, target_placeholder: y}
   true_count = sess.run(eval_correct, feed_dict=feed_dict)
   precision = float(true_count) / num_examples
   logloss = sess.run([loss],feed_dict=feed_dict)
   ll = np.mean(logloss)
   g_acc = precision
   g_logloss = ll
   print('examples: %d correct: %d  Precision: %0.05f Logloss: %0.06f' %
        (num_examples, true_count, precision, ll))


def run_training(xbatch_train, ybatch_train, xbatch_valid, ybatch_valid, xtest, xtrain, ytrain, xvalid, yvalid):
    with tf.Graph().as_default():
        global g_step, best_logloss, best_step
        features_placeholder, target_placeholder = placeholder_inputs()
        logits = model(features_placeholder)
        loss_val = loss(logits, target_placeholder)
        logloss_val = logloss(logits, target_placeholder)
        train_op = train(logloss_val)
        eval_correct = evaluation(logits, target_placeholder)
        summary = tf.summary.merge_all()
        init = tf.global_variables_initializer()
        saver = tf.train.Saver()
        sess = tf.Session()
        summary_writer = tf.summary.FileWriter(log_dir, sess.graph)
        sess.run(init)
        step = 0
        trial = 0
        while(step <= max_steps):
            step += 1
            g_step = step
            start_time = time.time()
            feed_dict = fill_feed_dict(xtrain, ytrain, features_placeholder, target_placeholder)
            _, loss_value, logloss_value = sess.run([train_op, loss_val, logloss_val], feed_dict=feed_dict)

            duration = time.time() - start_time
            if step % 100 == 0:
                acc = sess.run(eval_correct, feed_dict=feed_dict)/batch_size
                print('Step %d: loss = %.4f Logloss = %.4f Acc = %.2f (%.3f sec)' % (step, loss_value, logloss_value, acc, duration))
                summary_str = sess.run(summary, feed_dict=feed_dict)
                summary_writer.add_summary(summary_str, step)
                summary_writer.flush()

            if (step + 1) % 1000 == 0 or (step + 1) == max_steps:
                checkpoint_file = os.path.join(log_dir, 'model.ckpt')
                
                print('Training Data Eval:')
                do_eval('train_', sess, eval_correct, logloss_val, features_placeholder, target_placeholder, xtrain, ytrain)
                if(g_logloss <  best_logloss or True):
                    best_logloss = g_logloss
                    best_step = step
                    #print("New Best LL = " + str(best_logloss))
                    #trial = 0
                    #saver.save(sess, checkpoint_file, global_step=step)
                elif(g_logloss >  best_logloss + trial):
                    trial += .0001
                    print("Revert to " + str(best_step) + " Try " + str(trial) + " LL = " +str(best_logloss))
                    step = best_step
                    saver.restore(sess, tf.train.latest_checkpoint(log_dir))
                print('Validation Data Eval:')
                do_eval('valid_', sess, eval_correct, logloss_val, features_placeholder, target_placeholder, xvalid, yvalid)
                tf.summary.scalar('test', 1)
                summary_str = sess.run(summary, feed_dict=feed_dict)
                summary_writer.add_summary(summary_str, step)
                summary_writer.flush()

        predictions = sess.run(logits, feed_dict = {features_placeholder: xtest})

        return predictions

def dataSq(data):
    out = []
    for line in data:
        lineTall = np.array(line)
        lineTall.shape = (1,50)
        lineFlat = np.array(line)
        lineFlat.shape = (50,1)
        dot = np.dot(lineFlat, lineTall)
        dot.shape = (441)
        out.append(dot)
    return out

def main():
    start = time.time()
    np.random.seed(0)
    print("learning_rate = " + str(learning_rate))
    print("decay_rate = " + str(decay_rate))
    print("batch_size = " + str(batch_size))
    print("max_steps = " + str(max_steps))   
    print("model = " + str(layer_sizes))
    print('Loading data...')
    # Load the data from the CSV files
    training_data = pd.read_csv(datapath + 'numerai_training_data.csv', header=0)
    prediction_data = pd.read_csv(datapath + 'numerai_tournament_data.csv', header=0)

    # Transform the loaded CSV data into numpy arrays
    features = [f for f in list(training_data) if "feature" in f]
    X = training_data[features]
    Y = training_data["target"]
    x_prediction = prediction_data[features]
    ids = prediction_data["id"]

    #get validation data
    label = prediction_data["data_type"]
    label_list = label.values.tolist()
    val_cout = 0
    for item in label_list:
      if item == "validation" :
        val_cout += 1

    #separate dataset
    x_train = X.values.tolist()
    y_train = Y
    x_valid = x_prediction[:val_cout].values.tolist()
    y_valid = prediction_data["target"][:val_cout]
    x_test = x_prediction.values.tolist()

    #create batches
    batch_features_train = Batch(x_train, size = batch_size)
    batch_target_train = Batch(y_train.values.tolist(), size = batch_size)
    batch_features_valid = Batch(x_valid, size = batch_size)
    batch_target_valid = Batch(y_valid.values.tolist(), size = batch_size)

    print('Training...')
    predictions = run_training(batch_features_train, batch_target_train, batch_features_valid, batch_target_valid, x_test, x_train, y_train, x_valid, y_valid)

    print('Training complete, predictions obtained')
    results_df = pd.DataFrame(data={'probability':predictions})
    if not os.path.exists(os.path.dirname(prediction_path)):
        try:
            os.makedirs(os.path.dirname(prediction_path))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    joined = pd.DataFrame(ids).join(results_df)

    print("Writing predictions to " +  prediction_path)
    joined.to_csv(prediction_path, index=False)
    print("Done")
    stop = time.time()
    min = (stop - start)//(60)
    sec = ((stop - start)%60)//1
    print(('%d min %d sec') % (min, sec))
    print(('%0.05f mean, %0.05f std') % (np.mean(predictions), np.std(predictions)))

def wrapper(i_learning_rate = .001, i_batch_size = 100, i_max_steps = 10000, i_decay_rate = .001, i_layer_sizes = layer_sizes):
    global learning_rate, batch_size, max_steps, decay_rate, layer_sizes, best_step, best_logloss
    learning_rate = i_learning_rate
    batch_size = i_batch_size
    max_steps = i_max_steps
    decay_rate = i_decay_rate
    layer_sizes = i_layer_sizes
    best_step = 0
    best_logloss = 100
    main()
    return g_logloss, g_acc


if __name__ == '__main__':
    main()
