#model functions
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
#data imported as numpy arrays
import numpy as np
#used to allow saving/reading local files
import sys
sys.path.insert(0, '../')
#helper functions for plotting, data access, writing results
#needs to be after the sys.path.insrt to be read
import numnetHelper as helper

#setting params here to easily change and find
params = dict()
params['batch_size'] = 16384
params['epochs'] = 200
params['path'] = '../'
params['data'] = None
params['plot_loss_logloss'] = True
params['print_history_summary'] = True
params['save_pred'] = True
params['check_eras'] = True

def run(params = params):
    if params['data'] is None:
        x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
        params['data_in_dim'] = len(x_train[0])
    else:
        x_train, y_train, x_valid, y_valid, x_predict, ids = params['data']


    model = Sequential()
    model.add(Dense(units=2048, activation='relu', input_dim=params['data_in_dim']))
    model.add(Dropout(.5))
    #model.add(Dense(units=1024, activation='relu'))
    #model.add(Dense(units=1024, activation='relu'))
    #model.add(Dense(units=1024, activation='relu'))
    #model.add(Dense(units=1024, activation='relu'))
    #model.add(Dense(units=1024, activation='relu'))
    #model.add(Dense(units=1024, activation='relu'))
    model.add(Dense(units=1, activation='sigmoid'))


    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    earlyStop = EarlyStopping(monitor='val_loss', min_delta=0, patience=30, verbose=1)
    reduceLR = ReduceLROnPlateau(verbose=1)
    history = model.fit(x_train, y_train, verbose=2, epochs=params['epochs'], batch_size=params['batch_size'], validation_data=(x_valid, y_valid), callbacks=[earlyStop, reduceLR])

    prediction = model.predict(x_predict, verbose=1)

    if params['save_pred']:
        helper.savePred(ids, prediction, "keras_simple_1", params['path'])
    if params['print_history_summary']:
        helper.kerasHistorySummary(history)
    if params['plot_loss_logloss']:
        helper.kerasPlotHistory(history)
    if params['check_eras']:    
        helper.checkEras(ids, prediction, params['path'] )

if __name__=="__main__":
    runNet()